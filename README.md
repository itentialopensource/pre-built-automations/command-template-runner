<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 04-18-2024 and will be end of life on 04-18-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Configuration Management](https://gitlab.com/itentialopensource/pre-built-automations/iap-configuration-management)


<!-- Update the below line with your pre-built name -->
# Command Template Runner

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [How to Install](#how-to-install)
* [Input Variables](#input-variables)
* [How to Run](#how-to-run)

## Overview

<!-- Write a few sentences about the pre-built and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

This pre-built is a light solution to provide the ability for command templates to be run with little external assitance. More specifically, this solution provides built-in error handling that allows for retrying if necessary. This is necessary for workflows that may utilize multiple different command templates and reduces the need for further input. This workflow is intended to be run in a workflow through a ChildJob task, requiring an existing command template that needs to be run in a workflow as well as the device to run the command template on.

This solution consists of the following:
* Workflow (**Command Template Runner**)


<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->

<!-- REPLACE COMMENT ABOVE WITH IMAGE OF YOUR MAIN WORKFLOW -->

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_: 0-10 minutes (largely dependent on the Command Template that is being run)

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2023.1.x`

## Requirements

This pre-built requires the following:

<!-- Unordered list highlighting the requirements of the pre-built -->
<!-- EXAMPLE -->
<!-- * cisco ios device -->
<!-- * Ansible or NSO (with F5 NED) * -->
* Existing command templates
* A workflow that requires the utilization of command templates

## Features

The main benefits and features of the pre-built are outlined below.

<!-- Unordered list highlighting the most exciting features of the pre-built -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->
  * Provides a standardized method of running command templates with little input
  * Sufficient error handling to allow the user to see errors and retry if desired
  * Two modes of operation: Zero-Touch and Verbose
  * Proper outcome variables so that the parent workflow can easily handle the command template results

  _Example Scenarios_:
  * Upgrading a device - `install all [...]`
  * Checking device version - `show version`
  * Check device contents - `show dir`
  * Verify device contents - `install verify packages`
  * Remove device packages - `install remove inactive all`

## How to Install

To install the pre-built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the pre-built. 
* The pre-built can be installed from within Admin Essentials app. Simply search for the name of your desired pre-built and click the install button.

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this pre-built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## Input Variables
* Template Name - A string containing the name of the command template
* Template Variables - An object containing the variables to be used by the command template
* Devices - An array of strings containing the name(s) of the target device(s) on which the command template will be executed against
* Zero Touch - A boolean to determine whether or not the command template results should be shown
* Failure Type - A string variable to display the severity of failure in the case the command template does not pass (e.g. skip or abort)

_Example_:
```json
{
  "templateName": "Cisco NX-OS Upgrade - Pre Checks",
  "templateVariables": {
    "variable1": "variable1",
    "variable2": "variable2"
  },
  "devices": [
    "device1",
    "device2",
    "device3"
  ],
  "zeroTouch": true,
  "failureType": "abort"
}
```

## How to Run

* Navigate to any workflow that necessitates the use of a command template
* Place the **ChildJob** task on the canvas
* Select **Command Template Runner** and fill in the variables as desired, ensuring that the input command template name exists in the environment
* Run the workflow as desired
